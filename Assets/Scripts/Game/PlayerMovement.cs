﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 10f;

    private void Update()
    {
        var c = GetComponent<CharacterController>();

        var v = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
        {
            v += Vector3.left;
        }
        if (Input.GetKey(KeyCode.W))
        {
            v += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.D))
        {
            v += Vector3.right;
        }
        if (Input.GetKey(KeyCode.S))
        {
            v += Vector3.back;
        }
        c.SimpleMove(v * Speed);

        if (v.magnitude > 0.1)
        {
            transform.rotation = Quaternion.LookRotation(v);
        }
    }
}
