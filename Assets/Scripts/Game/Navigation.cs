using System;
using UnityEngine;

public class Navigation : MonoBehaviour
{
    protected TaskList TaskList;

    protected GameObject NavigatorGo;

    public Texture2D texture;

    private void Awake()
    {
        NavigatorGo = transform.Find("Navigator").gameObject;
        NavigatorGo.gameObject.GetComponent<MeshRenderer>().material.mainTexture = texture;
    }

    private void Start()
    {
        TaskList = FindObjectOfType<TaskManager>().TaskList;
    }

    private void Update()
    {
        if (TaskList.ActiveTask != null && TaskList.ActiveTask.NextActivity != null)
        {
            PointToActivity(TaskList.ActiveTask.NextActivity);
        }
    }

    private void PointToActivity(Activity activity)
    {
        var targetPosition = activity.InteractiveObject.gameObject.transform.position;
        var direction = targetPosition - transform.position;
        direction.y = 0;
        var distanceSq = direction.x * direction.x + direction.z * direction.z;
        direction.Normalize();
        var lookRotation = Quaternion.LookRotation(direction);
        NavigatorGo.transform.SetPositionAndRotation(transform.position + direction, lookRotation);
        var fadeDistance = 30;
        var minDistance = 20;
        if (distanceSq < fadeDistance)
        {
            var opacity = Math.Max(0, (distanceSq - minDistance) / (fadeDistance - minDistance));
            var color = NavigatorGo.GetComponent<MeshRenderer>().material.color;
            color.a = opacity;
            NavigatorGo.GetComponent<MeshRenderer>().material.color = color;
        }
        else
        {
            var color = NavigatorGo.GetComponent<MeshRenderer>().material.color;
            color.a = 1f;
            NavigatorGo.GetComponent<MeshRenderer>().material.color = color;
        }
    }
}