﻿using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NPC : MonoBehaviour
{
    public Transform Home;
    public InteractableType[] AllowedTargetTypes = new InteractableType[] { InteractableType.OfficeDesk, InteractableType.CoffeeMachine, InteractableType.Cabinet, InteractableType.Printer, InteractableType.FoodSource };
    private NavMeshAgent Agent;
    private Transform Target;

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        Initialize();
        FindNext();
    }

    private void Initialize()
    {
        NavMeshHit hit;
        var v2 = Random.insideUnitCircle * 30f;
        var v3 = new Vector3(v2.x, 0, v2.y);
        if (NavMesh.SamplePosition(v3, out hit, 25f, 1))
        {
            transform.position = hit.position;
        }
    }

    private void FindNext()
    {
        Transform next = null;
        if (Target != Home)
        {
            next = Home;
        }
        else
        {
            var possibleTargets = FindObjectsOfType<Interactable>().Where(i => AllowedTargetTypes.Contains(i.InteractableType)).ToArray();
            next = possibleTargets[Random.Range(0, possibleTargets.Length)].transform;
        }

        Agent.SetDestination(next.transform.position);
    }

    private void Update()
    {
        float dist = Agent.remainingDistance;
        if (dist != Mathf.Infinity && Agent.pathStatus == NavMeshPathStatus.PathComplete && Agent.remainingDistance < 0.5f)
        {
            FindNext();
        }
    }
}
