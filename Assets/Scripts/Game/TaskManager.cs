﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TaskListChanged : UnityEvent<TaskList, Task> { }
[System.Serializable]
public class ActivityCompleted : UnityEvent<Activity> { }

public class TaskManager : MonoBehaviour
{
    public TaskList TaskList { get; private set; }
    public TaskListChanged TaskListChanged;
    public ActivityCompleted ActivityCompleted = new ActivityCompleted();
    private List<Task> AvailableTasks = new List<Task>();

    private void Awake()
    {
        TaskList = new TaskList(this);
    }

    private void Start()
    {
        var taskDef = (TextAsset)Resources.Load("tasks", typeof(TextAsset));
        var definedTasks = JsonConvert.DeserializeObject<List<Task>>(taskDef.text);
        InitializeTasks(definedTasks);
        StartCoroutine(TaskCreator());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            AssignTask();
        }
    }

    private System.Collections.IEnumerator TaskCreator()
    {
        AssignTask();
        yield return new WaitForSeconds(UnityEngine.Random.Range(45, 120));
        StartCoroutine(TaskCreator());
    }

    public void HandleInteractableInteraction(Interactable interactable)
    {
        var activity = TaskList.ActivityForInteractable(interactable);
        if (activity != null)
        {
            activity.OnComplete();
        }
    }

    public void AssignTask()
    {
        if (AvailableTasks.Count < 1)
        {
            return;
        }
        var index = UnityEngine.Random.Range(0, AvailableTasks.Count);
        var task = AvailableTasks[index];
        TaskList.AddTask(task);
        TaskListChanged?.Invoke(TaskList, task);
    }

    public void InitializeTask(Task task)
    {
        var interactables = FindObjectsOfType<Interactable>();
        var taskOkay = true;
        var usedInteractables = new List<Interactable>();
        foreach (var activity in task.Activities)
        {
            activity.Reset();
            activity.Task = task;
            var interactableCandidates = activity.InteractableSelector
                .Split(',')
                .Select(term => term.Trim())
                .Aggregate(interactables, (carry, term) =>
                    carry.Where(interactable =>
                    {
                        var isReference = term.StartsWith("&");
                        if (isReference)
                        {
                            var id = int.Parse(term.Substring(1));
                            if (usedInteractables.Count >= id)
                            {
                                return interactable == usedInteractables[id];
                            }
                        }
                        if (usedInteractables.Contains(interactable))
                        {
                            return false;
                        }
                        return term.StartsWith(".")
                            ? interactable.InteractableType.ToString() == term.Substring(1)
                            : interactable.name == term.Substring(1);
                    }
                ).ToArray());
            var selectedIndex = UnityEngine.Random.Range(0, interactableCandidates.Length);
            if (interactableCandidates.Length == 0)
            {
                taskOkay = false;
                Debug.LogWarning($"[TaskManager] Coudn't find an Interactable matching the query {activity.InteractableSelector}");
                break;
            }
            var interactiveObject = interactableCandidates[selectedIndex];
            usedInteractables.Add(interactiveObject);
            activity.InteractiveObject = interactiveObject;
        }
        if (!taskOkay)
        {
            Debug.LogWarning($"[TaskManager] Failed to initialize Task {task.Name}");
            return;
        }
        AvailableTasks.Add(task);
    }

    private void InitializeTasks(List<Task> tasks)
    {
        var queue = new List<Task>(tasks);
        queue.Shuffle();

        foreach (var task in queue)
        {
            InitializeTask(task);
        }
    }
}
