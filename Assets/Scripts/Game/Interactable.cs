﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public InteractableType InteractableType;

    public int InteractionCooldown = 0;

    public bool IsOnCooldown { get; protected set; }

    private void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("Interactable");
    }

    public bool Interact()
    {
        if (IsOnCooldown) {
            return false;
        }
        if (InteractionCooldown > 0) {
            StartCoroutine(ResetCooldownCoroutine());
        }
        FindObjectOfType<TaskManager>().HandleInteractableInteraction(this);
        return true;
    }

    IEnumerator ResetCooldownCoroutine()
    {
        IsOnCooldown = true;
        yield return new WaitForSeconds(InteractionCooldown);
        IsOnCooldown = false;
    }
}
