using System.Diagnostics;
using System.Collections.Generic;
using System;

public class TaskList
{
    public List<Task> Tasks { get; }

    public Task ActiveTask { get; protected set; }

    protected TaskManager taskManager;

    public TaskList(TaskManager taskManager)
    {
        this.taskManager = taskManager;
        this.Tasks = new List<Task>();
    }

    public Activity ActivityForInteractable(Interactable interactable)
    {
        if (ActiveTask.NextActivity != null && ActiveTask.NextActivity.InteractiveObject == interactable)
        {
            return ActiveTask.NextActivity;
        }
        // foreach (var task in Tasks)
        // {
        //     if (task.NextActivity == null)
        //     {
        //         continue;
        //     }
        //     if (task.NextActivity.InteractiveObject == interactable)
        //     {
        //         return task.NextActivity;
        //     }
        // }
        return null;
    }

    internal void CompleteActivity(Activity acitivity)
    {
        taskManager.ActivityCompleted.Invoke(acitivity);
    }

    public TaskList AddTask(Task task)
    {
        if (ActiveTask == null)
        {
            ActiveTask = task;
        }
        task.TaskList = this;
        Tasks.Add(task);
        return this;
    }

    public TaskList SetActiveTask(Task task)
    {
        UnityEngine.Debug.Log(task.Name);
        UnityEngine.Debug.Log(task.NextActivity.Name);
        UnityEngine.Debug.Log(task.NextActivity.InteractiveObject.name);
        ActiveTask = task;
        return this;
    }

    public void OnTaskComplete(Task task)
    {
        Tasks.Remove(task);
        ActiveTask = Tasks.Count > 0 ? Tasks[0] : null;
        taskManager.InitializeTask(task);
        if (taskManager.TaskListChanged != null)
        {
            taskManager.TaskListChanged.Invoke(this, null);
        }
    }
}