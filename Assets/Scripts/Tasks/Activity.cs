﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Activity
{
    public enum ActivityPriority
    {
        NONE,
        LOW,
        MEDIUM,
        HIGH,
        CRITICAL,
    }
    public string Name { get; }

    public string Description { get; }

    public bool Complete { get; private set; }

    public int Duration { get; }

    public int Deadline { get; }

    public string InteractableSelector { get; set; }

    public ActivityPriority Priority { get; }

    [Newtonsoft.Json.JsonIgnore]
    public Interactable InteractiveObject { get; set; }

    public StatusValues Cost { get; }

    public StatusValues Benefit { get; }

    [Newtonsoft.Json.JsonIgnore]
    public Task Task;

    public Activity(
        string name,
        string description,
        string InteractableSelector,
        ActivityPriority priority,
        StatusValues cost,
        StatusValues benefit,
        int duration,
        int deadline = 0
    )
    {
        this.Name = name;
        this.Description = description;
        this.Complete = false;
        this.InteractableSelector = InteractableSelector;
        this.Priority = priority;
        this.Duration = duration;
        this.Deadline = deadline;
        this.Cost = cost;
        this.Benefit = benefit;
    }

    internal void Reset()
    {
        Complete = false;
    }

    public void OnComplete()
    {
        Complete = true;
        Task.CompleteActivity(this);
    }
}
