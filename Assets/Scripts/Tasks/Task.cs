using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Task
{
    public List<Activity> Activities { get; }

    public string Name { get; }

    public int Deadline { get; protected set; }

    public bool Complete => NextActivity == null;

    [Newtonsoft.Json.JsonIgnore]
    public Activity NextActivity => Activities.FirstOrDefault(a => !a.Complete);

    [Newtonsoft.Json.JsonIgnore]
    public TaskList TaskList;

    public Task(string name, int deadline = 0)
    {
        this.Name = name;
        this.Deadline = deadline;
        this.Activities = new List<Activity>();
    }

    public Task AddActivity(Activity activity)
    {
        activity.Task = this;
        Activities.Add(activity);
        return this;
    }

    public void CompleteActivity(Activity acitivity)
    {
        TaskList.CompleteActivity(acitivity);
        if (Complete)
        {
            TaskList.OnTaskComplete(this);
        }
    }
}