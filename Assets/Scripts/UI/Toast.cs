﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toast : MonoBehaviour
{
    public Text TextComponent;

    private bool active;

    public void ShowMessage(string message, int duration)
    {
        if (active) {
            StopCoroutine(ShowMessageCor(message, duration));
        }
        StartCoroutine(ShowMessageCor(message, duration));
    }

    private IEnumerator ShowMessageCor(string message, int duration)
    {
        active = true;
        Color originalColor = TextComponent.color;
        TextComponent.text = message;
        TextComponent.enabled = true;
        yield return fadeInAndOut(TextComponent, true, 0.5f);
        float counter = 0f;
        while (counter < duration) {
            counter += Time.deltaTime;
            yield return null;
        }
        yield return fadeInAndOut(TextComponent, false, 0.5f);
        TextComponent.enabled = false;
        TextComponent.color = originalColor;
        active = false;
    }

    private IEnumerator fadeInAndOut(Text text, bool fadeIn, float duration)
    {
        float a, b;
        if (fadeIn) {
            a = 0f;
            b = 1f;
        } else {
            a = 1f;
            b = 0f;
        }
        Color currentColor = Color.clear;
        float counter = 0f;
        while (counter < duration) {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);
            text.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
            yield return null;
        }
    }
}
