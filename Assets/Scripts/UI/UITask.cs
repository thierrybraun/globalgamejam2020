﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITask : MonoBehaviour
{
    private Task trackedTask;
    public Task TrackedTask
    {
        get
        {
            return trackedTask;
        }
        set
        {
            trackedTask = value;
            UpdateContent();
        }
    }
    public UIActivity ActivityPrefab;
    private TextMeshProUGUI Title;
    private TextMeshProUGUI Description;
    private Transform ActivityList;
    private TaskManager TaskManager;

    private void Awake()
    {
        Title = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        Description = transform.Find("Description").GetComponent<TextMeshProUGUI>();
        ActivityList = transform.Find("ActivityList");
        TaskManager = FindObjectOfType<TaskManager>();
    }


    private void Update()
    {
        if (trackedTask == null) return;

        ActivityList.gameObject.SetActive(TaskManager.TaskList.ActiveTask == trackedTask);
    }

    private void UpdateContent()
    {
        Title.text = trackedTask.Name;
        Description.text = trackedTask.NextActivity?.Description ?? "";
        for (int i = ActivityList.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(ActivityList.GetChild(i).gameObject);
        }
        foreach (var a in trackedTask.Activities)
        {
            var obj = Instantiate(ActivityPrefab).GetComponent<UIActivity>();
            obj.transform.SetParent(ActivityList);
            obj.TrackedActivity = a;
        }
    }

    public void SetAsActive()
    {
        TaskManager.TaskList.SetActiveTask(trackedTask);
    }
}
